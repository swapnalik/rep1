@RestResource(urlMapping='/CreateAccount/*')
Global with Sharing Class AccountCreate{
 @httpPost
  Global Static String createSingleAccount(String Name)
 {
   Account acc=new Account();
   acc.Name=Name;
   insert acc;
   
   return acc.id;
   
 }
}