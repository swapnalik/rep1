public class Reg{

//Registration__c R=new Registration__c();
private ApexPages.StandardController stdcontroller;
public String currentRecordId {get;set;}
   Public Registration__c  r{get;set;}
   Public String Gender{set;get;}
    
    public Reg(ApexPages.StandardController controller) {
     r = (Registration__c)controller.getrecord(); 
    }

    public List<SelectOption> getItems() {
       List<SelectOption> options = new List<SelectOption>(); 
       options.add(new SelectOption('Male','Male')); 
       options.add(new SelectOption('Female','Female')); 
       return options; 
   }
   
   Public PageReference RegCancel(){  
   
   Schema.DescribeSObjectResult anySObjectSchema = Registration__c.SObjectType.getDescribe();
        String objectIdPrefix = anySObjectSchema.getKeyPrefix();
        PageReference pageReference = new PageReference('/'+objectIdPrefix+'/o');
        pageReference.setRedirect(true);
        return pageReference;
        
  /* PageReference pg=New PageReference('/a00/o');        
   return pg; */
     }
     
    public PageReference Regsave() {
     r.Gender__c = Gender;
     insert r;
     
     /*currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
     system.debug('id****'+currentRecordId);*/
     PageReference pageReference=new PageReference('/'+r.id);
      
        pageReference.setRedirect(true);
        return pageReference;
    }

     
     
}